import json
import socket
import sys
import math
import pdb
import time
#from turbo import Turbo
from switch import Switch

class NoobBot(object):
    curr_rad = 0
    whole_angle = 0
    turbo_obj = 0
    turbo_available = False
    Magic = 0
    Position = 0
    Hamujemy = 0
    MyAngle = 0
    radius = 0
    speed = 0
    track = []
    current_piece = 0
    current_piece_accel = True
    last_pos = 0
    start_of_curve = 0
    max_angle = 0
    braking = False
    changed = 0
    min_speed = 0
    modifier = 0.99
    max_angle_lap = 0
    friction = 50

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

    def msg(self, msg_type, data):
        #print 'sent ', msg_type, data
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("joinRace", 
            { 
                "botId": 
                { 
                        "name": "DreamCarVSL", 
                        "key": "PtrkWAPblzFJoA" 
                }, 
                "trackName": "keimola",
                "carCount": 1
                }
            )
        # return self.msg("join", {"name": self.name,
        #                         "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)
        
    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        #self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def changed_curve(self, curr_angle, curr_pos):
        print "CHANGED CURVE"
        angle = abs(self.max_angle)
        if angle < 10:
            print "zwiekszam jak nic"
            multiplier = 1.5
        elif angle < 20:
            print "zwiekszam dosc mocno"
            multiplier = 1.1
        elif angle < 25:
            print "zwiekszam trocke"
            multiplier = 1.05
        else:
            print "nie zwiekszam"
            multiplier = 1
        
        for i in range(self.start_of_curve, curr_pos):
            if self.track[i]["speed"]!=-1:
                self.track[i]["speed"] *= multiplier
        self.start_of_curve = curr_pos
        self.max_angle = curr_angle


    def get_turn_angle(self, piece_index):
        radius = self.track[piece_index]["radius"]
        angle = 0
        i = 0

        while 1:
            try:
                if self.track[piece_index+i]["radius"] == radius:
                    angle += self.track[piece_index+i]["angle"]
                else:
                    return abs(angle)
                i+=1
            except KeyError:
                return abs(angle)


    def get_actual_radius(self, in_radius, piece_index, lane_index):
        radius = in_radius
        offset = self.lanes[lane_index]["distanceFromCenter"]
        angle = self.track[piece_index]["angle"]
        # print radius
        # print json.dumps(self.lanes, indent=2)
        # print self.lanes[lane_index]["distanceFromCenter"]
        if angle > 0:
            radius = in_radius - offset
        elif angle < 0:
            radius = in_radius + offset
        #print radius, piece_index
        return radius

    def get_next_turn(self, piece_index):
        i = piece_index
        out = 0
        while True:
            if i>=len(self.track):
                i=0
            try:
                rad = self.track[i]["radius"]
                if rad != self.curr_rad or out == 1:
                    self.whole_angle = self.get_turn_angle(i)
                    return i
            except KeyError:
                out = 1
            i+=1

    def accel_small_angle(self, angle):
        temp = 28*math.exp(-0.06*angle)
        #temp = 13.51354- 3.07348*math.log(angle)
        temp = 1.4
        #if abs(self.MyAngle) < 5:
            #return temp
        #else:
            #return 0
        return temp

    def should_brake(self, piece_index, lane_index):
        # if self.speed == 0:
        #     return False
        print('sb')
        try:
            if (self.track[
                    (piece_index-1)%len(self.track)
                    ]["radius"] == self.track[piece_index]["radius"]):
                if (self.track[
                        (piece_index-1)%len(self.track)
                        ]["angle"] == self.track[piece_index]["angle"]):
                    return False
        except:
            pass
        next_turn = self.get_next_turn(piece_index)
        rad = self.get_actual_radius(
            self.track[next_turn]["radius"], 
            next_turn, 
            Switch.getLaneIndex(next_turn))
        self.curr_rad=rad
        wanted_speed = self.count_wanted_speed(rad) + self.accel_small_angle(self.whole_angle)
        print(wanted_speed)
        
        speed_dif = -(wanted_speed - self.speed)
        #0.005 is experimental
        len_to_brake = speed_dif*self.friction #48.11
        left_to_go = self.track[piece_index%len(self.track)]["length"] - self.curr_pos
        len_to_next_turn = 0
        if piece_index > next_turn:
            next_turn += len(self.track)
        print (len_to_next_turn, piece_index, next_turn)
        for i in range(piece_index, next_turn):
            len_to_next_turn += self.track[i%len(self.track)]["length"]
        #print len_to_next_turn
        #print len_to_brake
        print('w_s={:.2f}, ltnt={:.2f}, ltb={:.2f}'.format(
            wanted_speed, len_to_next_turn, len_to_brake))
        if len_to_brake > len_to_next_turn and len_to_brake>0:
        #if len_to_brake+30 > len_to_next_turn > len_to_brake:
            print("SHOULD BRAKE")
            return True
        return False
        pass

    def count_wanted_speed(self, radius):
        #return 3.07776+0.0443529*radius-0.0000607746*(radius**2) #v1
        #return 3.09023+0.0436429*radius-0.0000575391*(radius**2) #v2
        if radius < 150:
            temp = self.modifier*(3.13185+0.0423812*radius-0.0000494667*(radius**2)) #v3
        else:
            temp = self.modifier*(-2*math.exp(0.008*radius)+2**0.5*radius**2/10000+0.04*radius+6) #v5
        # if radius == 40:
        #     temp = 0.96*temp
        return temp
        #return 3.13808+0.0420262*radius-0.0000478489*(radius**2) #v4
        #return 0.99*0.03*radius+3.85    

    def on_car_positions(self, data):


        self.curr_pos = data[0]["piecePosition"]["inPieceDistance"]
        piece_index = data[0]["piecePosition"]["pieceIndex"]
        current_angle = data[0]["angle"]
        self.MyAngle = current_angle
        lane_index = data[0]["piecePosition"]["lane"]["endLaneIndex"]
        if abs(current_angle) > self.max_angle:
            self.max_angle = abs(current_angle)
        
        

        #7.2
        if (not self.should_brake(piece_index+1, lane_index)):
            try:
                self.in_curve = True
                radius = self.track[piece_index]["radius"]
                print("ZAKRET TERAZ")
                #print json.dumps(data, indent=2)
                radius = self.get_actual_radius(radius, piece_index, lane_index)
                wanted_speed = self.count_wanted_speed(radius)
                print "radius={}, w_s={:.2f}".format(radius, wanted_speed)
            except KeyError:
                self.in_curve = False
                self.max_angle = 0
                wanted_speed = 10.0
                # if piece_index>35 and self.turbo_available:# and abs(current_angle)<10:
                #     self.turbo()
        else:
            print "braking"
            wanted_speed = 0.0
        #6.65
        #if self.turbo_obj.should_send():
            #print("COULD BE TURBO")



        # if self.in_curve:
        #     if abs(current_angle) > self.max_angle:
        #         self.max_angle = abs(current_angle)
        #     elif abs(current_angle) + 15 < self.max_angle:
        #         if wanted_speed != 0:
        #             print('going fast')
        #             wanted_speed == 2

        if self.speed < wanted_speed*0.95:
            # if wanted_speed - self.speed > 0.1:
            #     temp_throttle = 1
            # else:
            #    temp_throttle = wanted_speed/10
            temp_throttle = 1

        elif self.speed < wanted_speed:
            temp_throttle = wanted_speed/10
        else:
            # if self.speed - wanted_speed < 0.1:
            #     temp_throttle = wanted_speed/10
            # else:
            #   temp_throttle = 0
            temp_throttle = 0



        temp = self.curr_pos - self.last_pos
        if temp >= 0:
            self.speed = temp
        self.last_pos = self.curr_pos
        
        if not Switch.changeTrack(self, piece_index):
           self.throttle(temp_throttle)
        # self.throttle(temp_throttle)
        print("Speed={:.2f}({:.2f}) throttle={} angle={:.2f} piece={}".
            format(self.speed, wanted_speed, temp_throttle, current_angle, 
                piece_index))

    
    def on_game_init(self, data):
        #print json.dumps(data, indent=2)
        #exit()
        min_radius = 1000
        for i in data["race"]["track"]["pieces"]:
            # try:
            #     i["angle"] #to see if exists
            #     i["speed"] = 6.5
            #     if self.track[-1]["speed"] == 15:
            #         self.track[-1]["speed"] = -1 #dohamowanie
            # except KeyError:
            #     i["speed"] = 15
            try:
                i["length"]
            except:
                i["length"] = (i["radius"]+10)*i["angle"]*math.pi/180
                if i["radius"] < min_radius:
                    min_radius = i["radius"]
            self.track.append(i)
        #self.turbo_obj = Turbo(self.track)
        Switch.init(data)
        self.lanes = []
        for i in data["race"]["track"]["lanes"]:
            self.lanes.append(i)
        min_radius = min_radius - abs(self.lanes[0]["distanceFromCenter"])
        self.min_speed = self.count_wanted_speed(min_radius)
        print self.lanes
        print json.dumps(self.lanes, indent=2)
        print json.dumps(self.track, indent=2)
    '''
        i = 0
        for i in range(len(self.track)):
            try:
                print("kat="+str(self.track[i]["radius"]))
            except:
                print("kat=0")
    '''
        #for i in data["race"]["track"]["pieces"]:
            #try:
                #i["angle"]
                #i["throttle"] = 0.50    #poczatkowa wartosc. bedzie wieksze
                #if self.track[-1]["throttle"] == self.track[-2]["throttle"] == 1:
                    #self.track[-1]["throttle"] = 0.20   #dohamowanie
            #except:
                #i["throttle"] = 1   #grzejemy na prostej
            #self.track.append(i)

    def on_crash(self, data):
        print("Someone crashed\nspeed="+str(self.speed)+"\nangle="+str(self.MyAngle))
        self.modifier *= 0.97
        #exit()
        self.ping()

    def turbo(self):
        self.msg("turbo", "TUUUUUUUUUUUUUUUUUUUUUUUUUUURBO")
        self.turbo_available = False

    def on_turbo_available(self, data):
        self.turbo_available = True

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_lap_finish(self, data):
        print data
        if self.max_angle_lap < 40:
            self.modifier *= 1.01

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'turboAvailable': self.on_turbo_available,
            'gameInit': self.on_game_init,
            'lapFinished': self.on_lap_finish,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        print line
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                if msg_type == "lapFinished":
                    print(data)
                self.ping()
                #print data
            line = socket_file.readline()

if __name__ == "__main__":

    if False and len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        if len(sys.argv) == 5:
            host, port, name, key = sys.argv[1:5]
        else:
            host, port, name, key = "testserver.helloworldopen.com", "8091", "DreamCar", "PtrkWAPblzFJoA"
        print("Connecting with parameters:")
        #print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()