class Turbo(object):
    def __init__(self):
        self.act_points = []
        self.counting = False
        self.counting_start = 0
        self.first_lap = True
        self.tick_count = 0
        self.max_ticks = -1
        self.turbo_available = False
        self.turbo_length = 0
        self.turbo_activate = False

    def __str__(self):
        return str((self.act_points, self.counting, self.counting_start, self.first_lap, 
            self.tick_count, self.max_ticks, self.turbo_available, self.turbo_length))

    def crashed(self):
        self.tick_count = 0

    def should_send(self, piece_index, wanted_speed, on_curve):
        if not self.counting and self.max_ticks==-1:
            self.max_ticks = 0
            return False
        if self.first_lap or self.counting:
            if wanted_speed == 10:
                self.tick_count += 1
                if not self.counting:
                    self.counting_start = piece_index
                    self.counting = True
            else:
                if self.counting:
                    if piece_index in self.act_points:
                        self.act_points = [self.counting_start]
                    self.counting = False
                    if self.max_ticks <= self.tick_count:
                        if self.max_ticks < self.tick_count:
                            
                            self.act_points = [self.counting_start]
                            print 'asdf2', self.act_points
                        if self.max_ticks == self.tick_count:
                            print 'asdf1', self.act_points
                            self.act_points.append(self.counting_start)
                        self.max_ticks = self.tick_count
                        
                    self.counting_start = 0
                    self.tick_count = 0
                self.tick_count = 0
            return False
        else:
            if self.turbo_available and self.turbo_activate and not on_curve:
                print "to stad"
                self.turbo_activate = False
                return True
            if 0 in self.act_points:
                self.act_points = [self.counting_start]
            if self.turbo_available:
                if piece_index in self.act_points:
                    if on_curve:
                        self.turbo_activate = True
                        return False
                    return True
                else: 
                    return False

    def set_turbo(self, avail, data=[]):
        self.turbo_available = avail
        if avail:
            self.turbo_length = data["turboDurationTicks"]

    def end_lap(self):
        if self.first_lap:
            self.first_lap = False
        