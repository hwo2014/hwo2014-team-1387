#u gory dopisac from switch import Switch
#przed self.throttle() dodac Switch.changeTrack(self, piece_index)
#w on_game_init dodac Switch.init(self.track)
__author__ = 'Koral Piciorek'

import math
import json


class Switch:
    switchSegments = []
    lastSwitchIndex = -1 #zprzy kazdym switchu tylko 1 msg do servera
    laneNumber = 0

    @staticmethod
    def init(obj):
        Switch.track = obj["race"]["track"]["pieces"]
        Switch.laneNumber = len(obj["race"]["track"]["lanes"])
        seg_tmp = {}
        seg_tmp["startIndex"] = 0
        seg_tmp["track"] = "None"
        curvePriority = 0.0

        priority = lambda obj: 1/(2 * math.pi * obj["radius"] * 360 / obj["angle"])
        for i in range(0, len(Switch.track)):
            try:
                Switch.track[i]["switch"]
                seg_tmp["endIndex"] = i - 1
                if curvePriority > 0:
                    seg_tmp["track"] = "Right"
                elif curvePriority < 0:
                    seg_tmp["track"] = "Left"
                Switch.switchSegments.append(seg_tmp)
                seg_tmp = {}
                seg_tmp["startIndex"] = i
                seg_tmp["track"] = "None"
                curvePriority = 0.0
                #print("dupa: mam switch")
            except:
                try:
                    Switch.track[i]["angle"]
                    curvePriority += priority(Switch.track[i])

                    #print("dupa: zakret")
                except:
                    #print("dupa: prosta")
                    pass

        Switch.switchSegments[0]["startIndex"] = Switch.switchSegments[len(Switch.switchSegments)-1]["endIndex"] + 1

        for i in range(0, len(Switch.switchSegments)):
            if Switch.switchSegments[i]["track"] == "None":
                Switch.switchSegments[i]["track"] = Switch.switchSegments[Switch.findNextCurveSector(i)]["track"]


    @staticmethod
    def findNextCurveSector(inSector):
        sector = inSector
        while(True):
            sector += 1
            if sector >= len(Switch.switchSegments):
                sector = 0
            if Switch.switchSegments[sector]["track"] != "None":
                return sector

    @staticmethod
    def getSector(index):
        for i in range(1, len(Switch.switchSegments)):
            if Switch.switchSegments[i]["startIndex"] <= index <= Switch.switchSegments[i]["endIndex"]:
                return i
        return 0

    @staticmethod
    def changeTrack(obj, index):
        try:
            index += 1
            Switch.track[index]["switch"]
            if Switch.lastSwitchIndex == index:
                return
            Switch.lastSwitchIndex = index
            switchSector = Switch.getSector(index)

            if Switch.switchSegments[switchSector]["track"] != "None":
                obj.msg("switchLane", Switch.switchSegments[switchSector]["track"])
                print(str(switchSector) + "pykam" + Switch.switchSegments[switchSector]["track"])
                return True
            print("segmentIndex="+str(index) + " switchSector="+str(switchSector))
        except:
            pass
        return False

    @staticmethod
    def getLaneIndex(index):
        sector = Switch.getSector(index)
        if Switch.switchSegments[sector]["track"] == "Right":
            return 1
        else:
            return 0