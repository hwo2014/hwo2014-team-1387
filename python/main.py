import json
import socket
import sys
import math
import pdb
import time
from turbo import Turbo
from switch import Switch

class NoobBot(object):
    color = ""
    crash_number = 0
    my_len = 0
    curr_rad = 0
    whole_angle = 0
    turbo_obj = 0
    turbo_available = False
    Magic = 0
    Position = 0
    Hamujemy = 0
    MyAngle = 0
    radius = 0
    speed = 0
    track = []
    current_piece = 0
    current_piece_accel = True
    last_pos = 0
    start_of_curve = 0
    max_angle = 0
    braking = False
    changed = 0
    min_speed = 0
    modifier = 0.95
    max_angle_lap = 0
    friction = 50
    lap_counter = 0

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

    def msg(self, msg_type, data):
        #print 'sent ', msg_type, data
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        #Use commented thingy to test on different track
        #or race against someone
        # return self.msg("joinRace",
        #     {
        #         "botId":
        #         {
        #                 "name": "DreamCarVSL",
        #                 "key": "PtrkWAPblzFJoA"
        #         },
        #         "trackName": "france",
        #         "carCount": 1
        #         }
        #     )
        return self.msg("join", {"name": self.name,
                                  "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)
        
    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        #self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def changed_curve(self, curr_angle, curr_pos):
        print "CHANGED CURVE"
        angle = abs(self.max_angle)
        if angle < 10:
            print "zwiekszam jak nic"
            multiplier = 1.5
        elif angle < 20:
            print "zwiekszam dosc mocno"
            multiplier = 1.1
        elif angle < 25:
            print "zwiekszam trocke"
            multiplier = 1.05
        else:
            print "nie zwiekszam"
            multiplier = 1
        
        for i in range(self.start_of_curve, curr_pos):
            if self.track[i]["speed"]!=-1:
                self.track[i]["speed"] *= multiplier
        self.start_of_curve = curr_pos
        self.max_angle = curr_angle


    def get_turn_angle(self, piece_index):
        radius = self.track[piece_index]["radius"]
        angle = 0
        i = 0

        while 1:
            try:
                if self.track[piece_index+i]["radius"] == radius:
                    angle += self.track[piece_index+i]["angle"]
                else:
                    return angle
                i+=1
            except KeyError:
                return angle


    def get_actual_radius(self, in_radius, piece_index, lane_index):
        radius = in_radius
        offset = self.lanes[lane_index]["distanceFromCenter"]
        angle = self.track[piece_index]["angle"]
        # print radius
        # print json.dumps(self.lanes, indent=2)
        # print self.lanes[lane_index]["distanceFromCenter"]
        if angle > 0:
            radius = in_radius - offset
        elif angle < 0:
            radius = in_radius + offset
        #print radius, piece_index
        return radius

    def get_next_turn(self, piece_index):
        i = piece_index
        out = 0
        while True:
            if i>=len(self.track):
                i=0
            try:
                rad = self.track[i]["radius"]
                if rad != self.curr_rad or out == 1:
                    self.whole_angle = self.get_turn_angle(i)
                    return i
            except KeyError:
                out = 1
            i+=1

    def accel_small_angle(self, angle, rad):
        # temp = 0
        # # if abs(self.MyAngle) > 7:
        # #     if self.MyAngle*angle < 0:
        # #         temp += 0.05*self.MyAngle
        # #     else:
        # #         temp -= 0.1*self.MyAngle
        #
        # angle = abs(angle)
        # print ("WYBIERAM TEMP, RAD JEST ROWNY"+str(rad))
        # if rad < 50:
        #     temp += 0.3
        # elif rad < 70:
        #     temp += 0.6
        # elif rad < 80:
        #     temp += 0.95
        # elif rad < 100:
        #     temp += 0.88
        #     #temp = 1 dziala z 1.02 modifierem
        # elif rad < 120:
        #     temp = 1.22
        #     #temp += 1 dziala z 1.02 modifierem
        # elif rad < 150:
        #     temp += 1.2
        # else:
        #     temp += 0.6
        temp = self.modifier*(0.0000015*rad**2 + 0.0136179*rad - 0.5156023)
        if temp<0:
            temp = 0

        if self.MyAngle*angle < 0:
            if abs(self.MyAngle) > 40:
                temp += 0.8
            elif abs(self.MyAngle) > 30:
                temp +=0.5
            elif abs(self.MyAngle) > 20:
                temp += 0.25
            elif abs(self.MyAngle) > 10:
                temp += 0.1
        else:
            if abs(self.MyAngle) > 40:
                temp -= 0.8
            elif abs(self.MyAngle) > 30:
                temp -=0.5
            elif abs(self.MyAngle) > 20:
                temp -= 0.25
            elif abs(self.MyAngle) > 10:
                temp -= 0.1
        angle = abs(angle)
        if rad > 150:
            temp *= 0.75

        if angle < 10:
            temp *= 2
        elif angle < 15:
            temp *= 1.3
        elif angle < 20:
            temp *= 1.1

        return temp

    def should_brake(self, piece_index, lane_index):
        # if self.speed == 0:
        #     return False
        print('sb')
        try:
            if (self.track[
                    (piece_index-1)%len(self.track)
                    ]["radius"] == self.track[piece_index]["radius"]):
                if (self.track[
                        (piece_index-1)%len(self.track)
                        ]["angle"] == self.track[piece_index]["angle"]):
                    return False
        except:
            pass
        next_turn = self.get_next_turn(piece_index)
        rad = self.get_actual_radius(
            self.track[next_turn]["radius"], 
            next_turn, 
            Switch.getLaneIndex(next_turn))
        self.curr_rad=rad
        wanted_speed = self.count_wanted_speed(rad) + self.accel_small_angle(self.whole_angle, rad)
        print(wanted_speed)
        
        speed_dif = -(wanted_speed - self.speed)
        #0.005 is experimental
        len_to_brake = speed_dif*self.friction #48.11
        left_to_go = self.track[piece_index%len(self.track)]["length"] - self.curr_pos
        len_to_next_turn = 0
        if piece_index > next_turn:
            next_turn += len(self.track)
        print (len_to_next_turn, piece_index, next_turn)
        for i in range(piece_index, next_turn):
            len_to_next_turn += self.track[i%len(self.track)]["length"]
        #print len_to_next_turn
        #print len_to_brake
        print('w_s={:.2f}, ltnt={:.2f}, ltb={:.2f}'.format(
            wanted_speed, len_to_next_turn, len_to_brake))
        if len_to_brake > len_to_next_turn and len_to_brake>0:
        #if len_to_brake+30 > len_to_next_turn > len_to_brake:
            print("SHOULD BRAKE")
            return True
        return False
        pass

    def count_wanted_speed(self, radius):
        #return 3.07776+0.0443529*radius-0.0000607746*(radius**2) #v1
        #return 3.09023+0.0436429*radius-0.0000575391*(radius**2) #v2
        if radius < 150:
            temp = self.modifier*(3.13185+0.0423812*radius-0.0000494667*(radius**2)) #v3
            if radius > 80:
                temp *= 1.01
        else:
            temp = self.modifier*(-2*math.exp(0.008*radius)+2**0.5*radius**2/10000+0.04*radius+6) #v5
        # if radius == 40:
        #     temp = 0.96*temp
        return temp
        

        #return 3.13808+0.0420262*radius-0.0000478489*(radius**2) #v4
        #return 0.99*0.03*radius+3.85    

    def end_of_curve(self, piece_index, radius, in_piece_dist):
        try:
            if radius + 50 > self.track[(piece_index+1)%len(self.track)]["radius"]:
                return 1000
        except:
            pass
        whole_distance = abs(radius*2*math.pi*self.track[piece_index]["angle"]/360)
        return (whole_distance - in_piece_dist)/2/math.pi/radius*360

    def on_car_positions(self, data):
        car_index = 0
        for i in range(len(data)):
            if data[i]["id"]["color"] == self.color:
                car_index = i
                break
        print car_index
        self.curr_pos = data[car_index]["piecePosition"]["inPieceDistance"]
        piece_index = data[car_index]["piecePosition"]["pieceIndex"]
        current_angle = data[car_index]["angle"]
        self.MyAngle = current_angle
        if self.max_angle_lap < abs(self.MyAngle):
            self.max_angle_lap = abs(self.MyAngle)
        lane_index = data[car_index]["piecePosition"]["lane"]["endLaneIndex"]
        if abs(current_angle) > self.max_angle:
            self.max_angle = abs(current_angle)
        
        
        if self.speed==0:
            self.turbo_obj.crashed()
        #7.2
        if (not self.should_brake(piece_index+1, lane_index)):
            try:
                self.in_curve = True
                radius = self.track[piece_index]["radius"]
                print("ZAKRET TERAZ")
                #print json.dumps(data, indent=2)
                radius = self.get_actual_radius(radius, piece_index, lane_index)
                wanted_speed = self.count_wanted_speed(radius)
                angle_left = self.end_of_curve(piece_index, radius, data[0]["piecePosition"]["inPieceDistance"])
                if angle_left > 17 and self.MyAngle>43 and self.speed > wanted_speed:
                    wanted_speed = 0
                if angle_left > 12 and self.MyAngle>45 and self.speed > wanted_speed:
                    wanted_speed = 0
                if angle_left > 8 and self.MyAngle>49:
                    wanted_speed = 0
                if angle_left > 4 and self.MyAngle>54:
                    wanted_speed = 0
                if wanted_speed != 0:
                    if angle_left < 25 and self.MyAngle<45:
                        wanted_speed = 10
                    if angle_left < 20 and self.MyAngle<50:
                        wanted_speed = 10
                    if angle_left < 15 and self.MyAngle<52:
                        wanted_speed = 10
                if self.MyAngle > 54:
                    self.modifier *= 0.9998
                    wanted_speed = 0

                # if wanted_speed == 10:
                #     print "WYJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAZD"
                print "radius={}, w_s={:.2f}".format(radius, wanted_speed)
            except KeyError:
                self.in_curve = False
                self.max_angle = 0
                wanted_speed = 10.0
                # if piece_index>35 and self.turbo_available:# and abs(current_angle)<10:
                #     self.turbo()
        else:
            print "braking"
            wanted_speed = 0.0
        #6.65

        try:
            self.track[piece_index]["angle"]
            self.in_curve = True
        except KeyError:
            self.in_curve = False

        if self.turbo_obj.should_send(
                wanted_speed=wanted_speed, 
                piece_index=piece_index,
                on_curve=self.in_curve):
            self.turbo()
            


        # if self.in_curve:
        #     if abs(current_angle) > self.max_angle:
        #         self.max_angle = abs(current_angle)
        #     elif abs(current_angle) + 15 < self.max_angle:
        #         if wanted_speed != 0:
        #             print('going fast')
        #             wanted_speed == 2

        if self.speed < wanted_speed*0.95:
            # if wanted_speed - self.speed > 0.1:
            #     temp_throttle = 1
            # else:
            #    temp_throttle = wanted_speed/10
            temp_throttle = 1

        elif self.speed < wanted_speed:
            temp_throttle = wanted_speed/10
        else:
            # if self.speed - wanted_speed < 0.1:
            #     temp_throttle = wanted_speed/10
            # else:
            #   temp_throttle = 0
            temp_throttle = 0

        #to make turbo ALWAYS WORK
        if wanted_speed == 10:
            temp_throttle = 1



        temp = self.curr_pos - self.last_pos
        if temp >= 0:
            self.speed = temp
        self.last_pos = self.curr_pos
        
        if not Switch.changeTrack(self, piece_index):
           self.throttle(temp_throttle)
        # self.throttle(temp_throttle)
        print("Speed={:.2f}({:.2f}) throttle={} angle={:.2f} piece={}".
            format(self.speed, wanted_speed, temp_throttle, current_angle, 
                piece_index))
        print str(self.turbo_obj)

    
    def on_game_init(self, data):
        #print json.dumps(data, indent=2)
        #exit()
        self.my_len = data["race"]["cars"][0]["dimensions"]["length"]
        print(str(self.my_len))
        min_radius = 1000
        for i in data["race"]["track"]["pieces"]:
            # try:
            #     i["angle"] #to see if exists
            #     i["speed"] = 6.5
            #     if self.track[-1]["speed"] == 15:
            #         self.track[-1]["speed"] = -1 #dohamowanie
            # except KeyError:
            #     i["speed"] = 15
            try:
                i["length"]
            except:
                i["length"] = (i["radius"]+10)*i["angle"]*math.pi/180
                if i["radius"] < min_radius:
                    min_radius = i["radius"]
            self.track.append(i)
        self.turbo_obj = Turbo()
        Switch.init(data)
        self.lanes = []
        for i in data["race"]["track"]["lanes"]:
            self.lanes.append(i)
        min_radius = min_radius - abs(self.lanes[0]["distanceFromCenter"])
        self.min_speed = self.count_wanted_speed(min_radius)
        print self.lanes
        print json.dumps(self.lanes, indent=2)
        print json.dumps(self.track, indent=2)
    '''
        i = 0
        for i in range(len(self.track)):
            try:
                print("kat="+str(self.track[i]["radius"]))
            except:
                print("kat=0")
    '''
        #for i in data["race"]["track"]["pieces"]:
            #try:
                #i["angle"]
                #i["throttle"] = 0.50    #poczatkowa wartosc. bedzie wieksze
                #if self.track[-1]["throttle"] == self.track[-2]["throttle"] == 1:
                    #self.track[-1]["throttle"] = 0.20   #dohamowanie
            #except:
                #i["throttle"] = 1   #grzejemy na prostej
            #self.track.append(i)

    def on_crash(self, data):
        print("Someone crashed\nspeed="+str(self.speed)+"\nangle="+str(self.MyAngle))
        if data["color"] == self.color:
            if self.crash_number >= 2:
                self.modifier *= 0.985
                self.crash_number = 0
            else:
                self.crash_number += 1
            self.max_angle_lap = 60
        self.ping()

    def turbo(self):
        print('TURBOTURBOTURBO')
        print('TURBOTURBOTURBO')
        print('TURBOTURBOTURBO')
        print('TURBOTURBOTURBO')
        print('TURBOTURBOTURBO')
        self.msg("turbo", "TUUUUUUUUUUUUUUUUUUUUUUUUUUURBO")
        self.turbo_obj.set_turbo(False)
        self.turbo_available = False

    def on_turbo_available(self, data):
        self.turbo_obj.set_turbo(True, data)
        self.turbo_available = True

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_your_car(self, data):
        self.color = data["color"]

    def on_lap_finish(self, data):
        print data
        self.turbo_obj.end_lap()

        if self.max_angle_lap < 45:
            if self.lap_counter == 3:
                self.modifier *= 1.01
                self.lap_counter = 0
            else:
                self.lap_counter += 1
        # elif self.max_angle_lap < 58:
        #     if self.lap_counter == 3:
        #         self.modifier *= 1.005
        #         self.lap_counter = 0
        #     else:
        #         self.lap_counter += 1
        else:
            self.lap_counter = 0

        self.max_angle_lap = 0

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'turboAvailable': self.on_turbo_available,
            'gameInit': self.on_game_init,
            'lapFinished': self.on_lap_finish,
            'yourCar': self.on_your_car,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        print line
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                if msg_type == "lapFinished":
                    print(data)
                self.ping()
                #print data
            line = socket_file.readline()

if __name__ == "__main__":

    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()